# U.S. Web Design Standards - Paragraphs

INTRODUCTION
------------

This module, when enabled, installs several Paragraph types that
correspond to some more complex components from the U.S. Web Design
Standards library. They are:

* Hero with callout
* Graphic list of media blocks
* Two columns - one-third width and two-thirds width

After installation, feel free to alter the Paragraph types be adding
fields or altering fields. However, try not to remove fields.

REQUIREMENTS
------------

This module requires the [Paragraphs module](https://www.drupal.org/project/paragraphs).

RECOMMENDED THEME
-----------------

This module creates markup optimized for use with the
[U.S. Web Design System theme](https://www.drupal.org/project/uswds).

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
